## NOTA DE LIBERAÇÃO: ASSINADOR DIGITAL ICP - BRASIL: PLUGIN PARA NAVEGADORES WEB.	
###INTRODUÇÃO

O presente trabalho visa, o desenvolvimento de uma aplicação web para assinatura digital, a qual fará a comunicação com o software de assinatura digital ICP - Brasil, desenvolvido em parceria pelo Laboratório de Sistemas Integráveis Tecnológico – São Paulo/SP (LSI-TEC) e pelo Laboratório de Pesquisa em Computação e Sistemas de Informação (COMPSI). Onde o cliente selecionara o arquivo que se deseja assinar e todo o processo de assinatura será realizado localmente em background.
A linguagem JavaScript foi utilizada para realizar a comunicação entre a pagina Web com o assinador, onde O cliente selecionara o arquivo para ser assinado e no momento da assinatura a aplicação ira verificar se o computador possui o app.jar (Assinador ICP - Brasil), se ele possuir o app.jar todo o processo de assinatura digital desde o momento de selecionar o arquivo até a confirmação da assinatura sera armazenada em um arquivo log.txt, caso contrario o cliente tera a opção de realizar o download ou cancelar a operação, se resolver efetuar o download  sera executado um arquivo .bat onde o mesmo realiza uma requisição no servidor web e realiza o download do arquivo app.jar, esse arquivo é armazenado localmente no computador e descompactado para o endereço C:\. em seguida o cliente é alertado com o fim do download, podendo assim dar continuidade na operação. 
 No momento em que é executado o assinador app.jar, as informações armazenadas no arquivo log.txt, são visualizadas no navegador em momento de execução.

## 1.	NOTA DE RELEASE A SER PUBLICADO
- Efetuar Download da Aplicação
- Gerar arquivo Log com todos os Passos para concretizar a assinatura
## 2.	PROBLEMAS CONHECIDOS E LIMITAÇÕES
### Limitação
- O projeto é limitado para somente um navegador, o Internet Explorer, sendo realizado testes somente até a versão 10.
- Somente Plataforma Windows.
- O tempo de latência é Baixo.

## 3.	DATAS IMPORTANTES
Segue abaixo as datas importante do desenvolvimento:

Data | Evento
------------- | -------------
26/06/2015 | Início do planejamento
10/08/2015 | Início do desenvolvimento
18/11/2015 | Entrega para teste
20/11/2015 | Fim do teste
23/11/2015 | Liberação para produção

## 4.	COMPATIBILIDADE
Segue abaixo os requisitos:

Requisitos | Ferramentas
------------- | -------------
Navegadores | Internet Explorer
Sistema operacional | Windows Seven

#### Tecnologias

Tecnologia |
------------- | -------------
Linguagem de   Programação | Java, JavaScrip, Comandos DOS
Framework WEB | Apache Axis
IDE | NetBeans IDE – Java
Servidor | Web	Apache Axix, Apache Tomcat

## 5.	PROCEDIMENTO E ALTERAÇAO DE CONFIGURAÇÃO DO AMBIENTE
Há unica alteração do ambiente que precisa ser configurada para que a aplicação funcione perfeitamente, seria a instalação do Web Service Java Apache TomCat junto com o Framework Apache Axis, sendo necessario copiar a aplicação que esta compactada, para a pasta dentro do diretório Axis dentro do Tomcat.
Para que a aplicação Web consiga efetuar o Download  dos arquivo necessários, para realizar o processo de assinatura.

## 6.	ATIVIDADES REALIZADAS NO PERÍODO
Nessa liberação foram contemplados os seguintes itens:

Cód | Título | Tarefa |	Situação | Observação
------------- |------------- |------------- |------------- |------------- 
1 | Título da funcionalidade | Assinador Digital  ICP – Brasil: Plugin para Navegadores WEB | Concluído |	
2 | Comunicação entre Plataformas Diferentes | Realizado a comunicação entre a aplicação Web e o assinador digital ICP – Brasil, passando como parâmetro um endereço. | Concluído |	
3 | Executar a aplicação em Modo Background | Sendo desabilitado toda a parte gráfica da aplicação, quando é realizado uma chamada ao Assinador, ele é executado em segundo plano. | Concluído	
4 | Efetuar assinatura em modo Background | Realizar todo o procesos de assinatura de um documento digital em modo Backgroud | Não Concluido | Devido a alteração do endereço da lista de Certificados revogados no repositório
5 | Arquivo Log	| Inserir Principais Informações necessárias para concretizar a assinatura de um documento | Concluido |	
6 | Download Assinador | Caso o cliente não possua o Assinador em sua maquina local, terá a opção de efetuar o Download da mesma. |	Concluido |